#include <GL/freeglut.h>

void drowLevy(int x1, int y1, int x2, int y2, int i)
{
    if(i == 0) {
        glColor3f(x1 * 0.01, x2 * 0.002, i * 0.03);
        glVertex2i(x1, y1); //���������� ���������������
        glVertex2i(x2, y2); //�������
    }
    else {
        int x3 = (x1 + x2) / 2 - (y2 - y1) / 2; //����������
        int y3 = (y1 + y2) / 2 + (x2 - x1) / 2; //����� ������
        drowLevy(x1, y1, x3, y3, i - 1);
        drowLevy(x3, y3, x2, y2, i - 1);
    }
}

void reshape(int w, int h)
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glViewport(0, 0, w, h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, w, 0, h);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);

    glBegin(GL_LINES);
        drowLevy(300, 400, 500, 400, 20); //����� ������� ���������
    glEnd();

    glutSwapBuffers();
}

int main(int argc, char *argv[])
{
      glutInit(&argc, argv);
      glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
      glutInitWindowSize(800, 800);

      glutCreateWindow("Levy");

      glutDisplayFunc(display);
      glutReshapeFunc(reshape);

      glutMainLoop();
}
